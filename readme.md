# AJAXful routes for Laravel #

**AJAXful Routes** is a replacement class for the default *controller class* of the awesome [Laravel framework](http://www.laravel.com) version 3.x.

## Installation ##

The installation is simple and fast:

-  put the **controller.php** file from this repository into your **application/libraries** folder
- remove or comment out the alias to the controller class in your **application/config/application.php**

At this point, the class will be autoloaded (if you haven't removed the application/libraries folder from your autoloaded path library) and all the controllers will inherit the *ajaxful* properties from this class.

## Usage ##

Make a controller:

	<?php
	
	Class WelcomeController extends Controller
	
		public $restful = true; // Default controllers are not restful by default in Laravel
		public $ajaxful = true; // Set the ajaxful property to true, enabling this controller to act as an ajaxful controller
	
		public function get_index()
		{
			return View::make('home.index'); // this will be called if a non-ajax request will be made
		}
	
		public function ajax_get_index()
		{
			return View::make('home.ajax'); // this will be called if an ajax request will be made
		}
	
		public function post_index() // non-ajax call
		{
			return View::make('home.index');
		}
	
		public function ajax_post_index() //ajax call
		{
			return View::make('home.ajax');
		}
	}
`